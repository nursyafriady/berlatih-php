<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>
<body>
    <!-- Ketentuan :
    1. jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
    2. jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik”
    3. jika parameter integer lebih besar sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup”
    4. selain itu maka akan mereturn string “Kurang” -->
    <h1>Soal 1. Kategori Nilai</h1>
    <?php
        function tentukan_nilai($number)
            {
                if ($number >= 85 && $number <= 100) {
                    echo "Nilai ". $number . " : Sangat Baik <br>";
                } else if ($number >= 70 && $number < 85) {
                    echo "Nilai ". $number . " : Baik <br>";
                } else if ($number >= 60 && $number < 70) {
                    echo "Nilai ". $number . " : Cukup <br>";
                } else {
                    echo "Nilai ". $number . " : Kurang";
                }
            };

        //TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang
    ?>
</body>
</html>