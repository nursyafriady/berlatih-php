<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Huruf</title>
</head>
<body>
    <!-- 1. function akan mereturn string yang berisi karakter-karakter yang sudah diubah dengan karakter setelahnya dalam susunan    
            abjad“abdcde …. xyz”. 
         2. karakter “a” akan diubah menjadi “b” karakter “x” akan berubah menjadi “y”, dst. 
     -->

    <h1>Soal 2. Ubah Huruf</h1>
    <?php
        function ubah_huruf($string) {
            $abjad = ['a', 'b', 'c', 'd', 'e', 
                      'f', 'g', 'h', 'i', 'j', 
                      'k', 'l', 'm', 'n', 'o', 
                      'p', 'q', 'r', 's', 't', 
                      'u', 'v', 'w', 'x', 'y', 'z'];
            // $abjad = "abcdefghijklmnopqrstuvwxyz";
            // echo count($abjad);
            // echo $abjad.length
            // $abjad = array(range('a', 'z'));        
            // echo "<pre>";
            // print_r($abjad);
            // echo "</pre>";
            // foreach ($ar as $val) {
            //     $str .= implode($sep, $val);
            //     $str .= $sep; // add separator between sub-arrays    
            // }
            // print_r(str_split("Hello",1));

            $gantiChar = "";            
            foreach(str_split($string, 1) as $char) {
                for ($i=0; $i < count($abjad); $i++) {
                    if ($char === $abjad[$i]) {
                        $gantiChar .= $abjad[$i+1];                           
                    };
                };
            };

            echo "ubah_huruf ($string) : ";
            return $gantiChar; 
        };          
      
        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo "<br>";
        echo ubah_huruf('developer'); // efwfmpqfs
        echo "<br>";
        echo ubah_huruf('laravel'); // mbsbwfm
        echo "<br>";
        echo ubah_huruf('keren'); // lfsfo
        echo "<br>";
        echo ubah_huruf('semangat'); // tfnbohbu
    ?>
</body>
</html>